/**
 * TabHost是一种选项卡或标签的那种布局，里面有TabWidget(Tab标签)和FramLayout(Tab标签内容)
 * 1、其布局是固定的：TabHost包含TabWidget和FrameLayout
 * 2、注意其id是固定的：  <TabHost android:id="@android:id/tabhost"
 * 						<TabWidget android:id=@android:id/tabs
 * 						<FrameLayout android:id=@android:id/tabcontent
 * 3、而且Activity要继承TabActivity
 * @author LittleBoy
 * 2013-07-18 23:31
 */
public class MainActivity extends TabActivity {

	private TabHost mTabHost;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        //---1、取得TabHost
        mTabHost = getTabHost();
       
        //---2、设置TabWidget的布局
        View view1 = LayoutInflater.from(this).inflate(R.layout.layout_tabwidget, null);
        ImageView mImg1 = (ImageView)view1.findViewById(R.id.mImg);
        TextView mTitle1 = (TextView)view1.findViewById(R.id.mTitle);
        mImg1.setImageResource(R.drawable.ic_launcher);
        mTitle1.setText("tab1");
        
        View view2 = LayoutInflater.from(this).inflate(R.layout.layout_tabwidget, null);
        ImageView mImg2 = (ImageView)view2.findViewById(R.id.mImg);
        TextView mTitle2 = (TextView)view2.findViewById(R.id.mTitle);
        mImg2.setImageResource(R.drawable.ic_launcher);
        mTitle2.setText("tab2");
        
        View view3 = LayoutInflater.from(this).inflate(R.layout.layout_tabwidget, null);
        ImageView mImg3 = (ImageView)view3.findViewById(R.id.mImg);
        TextView mTitle3 = (TextView)view3.findViewById(R.id.mTitle);
        mImg3.setImageResource(R.drawable.ic_launcher);
        mTitle3.setText("tab3");
        
        //---给TabHost添加Tab（1、TabWidget和FrameLayout）
        //---TabWidget有自己的布局
        //---FrameLayout指Tab的内容，一般是Activity
        mTabHost.addTab(mTabHost.newTabSpec("tab1")
        		.setIndicator(view1)
        		.setContent(new Intent(MainActivity.this,TabActivity1.class)));
        
        
        mTabHost.addTab(mTabHost.newTabSpec("tab2")
        		.setIndicator(view2)
        		.setContent(new Intent(MainActivity.this,TabActivity2.class)));
        
        
        mTabHost.addTab(mTabHost.newTabSpec("tab3")
        		.setIndicator(view3)
        		.setContent(new Intent(MainActivity.this,TabActivity3.class)));
        
    }




   